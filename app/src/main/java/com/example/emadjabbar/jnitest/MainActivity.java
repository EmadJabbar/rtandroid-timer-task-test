package com.example.emadjabbar.jnitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    int i = 0;
    long[] times;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //run test native
        doTest();

        //run test on JVM
//        doTestInJvm();
    }



    public void doTestInJvm(){
        i = 0;
        times = new long[4000];

        Timer timerObj = new Timer();
        final TimerTask timerTaskObj = new TimerTask() {
            public void run() {
                Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
                i++;
                if (i <4000)
                    times[i] = System.nanoTime();
                if (i >= 4000){
                    for (int j=1; j<4000; j++){
                        Log.d("TIME", "time:  "+ (times[j]-times[j-1]) );
                    }
                    cancel();
                }
            }
        };
        timerObj.schedule(timerTaskObj, 0, 5);
    }



    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */

    public native void doTest();

}
