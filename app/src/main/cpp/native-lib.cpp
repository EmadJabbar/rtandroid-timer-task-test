#include <jni.h>
#include <string>
#include <android/log.h>
#include <thread>
#include <unistd.h>

#define DEBUG_TAG "NDK_AndroidNDK1SampleActivity"
const auto timeWindow = std::chrono::milliseconds(5);

bool flag = true;


int fib(int i) {
    if ( i==0 || i==1 )
        return 1;
    int res = fib(i-1) + fib(i-2);
    __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NDK:FIB:[%d] -> [%d]", i, res);

    return res;

}


void calFib(int i) {
    while (flag){
       fib(i);
    }
}


void doSomeThingPeriodic(int i) {
    nice( 120);
    long long int* t_stamps = new long long int[i];
    int j = 0;
    while (j<i){
        t_stamps[j] = std::chrono::steady_clock::now().time_since_epoch().count();
        j++;
        std::this_thread::sleep_for(timeWindow);
    }
    long long int sum =0 ;
    for (int z = 0; z < i; ++z) {
        if (z>0)
            sum += t_stamps[z]-t_stamps[z-1];
        __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NDK:Time:[%lli]",t_stamps[z]-t_stamps[z-1]);
    }
    __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NDK:Time:Total: [%lli]",sum);
    flag = false;
    delete t_stamps;
}

extern "C"
JNIEXPORT jstring
JNICALL
Java_com_example_emadjabbar_jnitest_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT void
JNICALL
Java_com_example_emadjabbar_jnitest_MainActivity_doTest(
        JNIEnv *env,
        jobject /* this */) {
    jboolean isCopy;
    jstring log = env->NewStringUTF("run test!!!");
    const char * szLogThis = (env)->GetStringUTFChars(log, &isCopy);
    __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NDK:LC: [%s]", szLogThis);
    std::thread first (doSomeThingPeriodic, 2000);
    first.join();
    __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NDK:Time:end");
    return;
}


extern "C"
JNIEXPORT void
JNICALL
Java_com_example_emadjabbar_jnitest_MainActivity_calFib(
        JNIEnv *env,
        jobject /* this */) {
    jboolean isCopy;
    jstring log = env->NewStringUTF("run fib thread!!!");
    const char * szLogThis = (env)->GetStringUTFChars(log, &isCopy);
    __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, "NDK:LC: [%s]", szLogThis);
    std::thread fib_thread (calFib, 50);
    fib_thread.join();
    return;
}










